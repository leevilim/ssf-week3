import { getConnectionType } from "../../domain/operations/connectionTypeOperations.js";
import { getLevel } from "./../../Domain/Operations/levelOperations.js";
import { getCurrentType } from "./../../domain/operations/currentTypeOperations.js";

export default {
  Connection: {
    LevelID: async (parent, _) => {
      return await getLevel(parent.LevelID);
    },
    ConnectionTypeID: async (parent, _) => {
      return await getConnectionType(parent.ConnectionTypeID);
    },

    CurrentTypeID: async (parent, _) => {
      return await getCurrentType(parent.CurrentTypeID);
    },
  },
};
