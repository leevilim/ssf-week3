import { getCurrentTypes } from "./../../domain/operations/currentTypeOperations.js";

export default {
  Query: {
    currenttypes: async (_, args) => {
      return await getCurrentTypes();
    },
  },
};
