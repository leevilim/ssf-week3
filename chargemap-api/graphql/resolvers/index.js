import connectionResolver from "./connectionResolver.js";
import connectionTypeResolver from "./connectionTypeResolver.js";
import currentTypeResolver from "./currentTypeResolver.js";
import levelResolver from "./levelResolver.js";
import stationResolver from "./stationResolver.js";
import userResolver from "./userResolver.js";

export default [
  connectionResolver,
  connectionTypeResolver,
  currentTypeResolver,
  levelResolver,
  stationResolver,
  userResolver,
];
