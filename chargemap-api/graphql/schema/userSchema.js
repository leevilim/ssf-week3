import { gql } from "apollo-server-express";

export default gql`
  extend type Query {
    users: [User]
  }

  type User {
    id: ID
    Username: String
    Password: String
    Full_name: String
  }
`;
