import { ApolloServer } from "apollo-server-express";
import typeDefs from "./graphql/schema/index.js";
import resolvers from "./graphql/resolvers/index.js";
import express from "express";
import db from "./utils/db.js";
import cors from "cors";

// import a from "./domain/model/connection.js";
// import b from "./domain/model/connectionType.js";
// import c from "./domain/model/currentType.js";
// import d from "./domain/model/level.js";
// import e from "./domain/model/station.js";
// import f from "./domain/model/user.js";

(async () => {
  try {
    const server = new ApolloServer({
      typeDefs,
      resolvers,
    });

    const app = express();
    app.use(cors({ origin: true }));
    app.use(express.json());
    app.use(express.urlencoded({ extended: true }));

    await server.start();

    server.applyMiddleware({ app });

    db.on("connected", () => {
      app.listen({ port: process.env.PORT || 3000 }, () =>
        console.log(
          `🚀 Server ready at http://localhost:3000${server.graphqlPath}`
        )
      );
    });
  } catch (e) {
    console.log("server error: " + e.message);
  }
})();
