import mongoose from "mongoose";
import { jsonOptions } from "../../utils/schemaOptions.js";

const levelSchema = new mongoose.Schema(
  {
    _id: mongoose.Schema.ObjectId,
    Comments: String,
    IsFastChargeCapable: Boolean,
    Title: String,
  },
  jsonOptions
);

export default mongoose.models.Level || mongoose.model("Level", levelSchema);
