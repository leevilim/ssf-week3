import mongoose from "mongoose";

const currentTypeSchema = new mongoose.Schema({
  _id: mongoose.Schema.ObjectId,
  Description: String,
  Title: String,
});

export default mongoose.model("CurrentType", currentTypeSchema);
