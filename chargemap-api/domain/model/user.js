import mongoose from "mongoose";

const userSchema = new mongoose.Schema({
  _id: mongoose.Schema.ObjectId,
  username: String,
  password: String,
  full_name: String,
});

export default mongoose.model("User", userSchema);
