import mongoose from "mongoose";

const connectionTypeSchema = new mongoose.Schema({
  _id: mongoose.Schema.ObjectId,
  FormalName: String,
  Title: String,
});

export default mongoose.models.ConnectionType ||
  mongoose.model("ConnectionType", connectionTypeSchema);
