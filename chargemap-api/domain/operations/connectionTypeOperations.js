import ConnectionType from "../model/connectionType.js";

export const getConnectionType = async (id) => {
  return await ConnectionType.findById(id);
};

export const getConnectionTypes = async () => {
  return await ConnectionType.find({});
};
