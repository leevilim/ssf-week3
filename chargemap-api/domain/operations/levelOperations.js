import Level from "../model/level.js";

export const getLevel = async (id) => {
  return await Level.findById(id);
};

export const getLevels = async () => {
  return await Level.find({});
};
