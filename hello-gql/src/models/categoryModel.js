import mongoose from "mongoose";

const categoryModel = new mongoose.Schema({
  _id: {
    type: mongoose.Schema.ObjectId,
    auto: true,
  },
  categoryName: String,
});

export default mongoose.model("Category", categoryModel);
