import mongoose from "mongoose";

const animalModel = new mongoose.Schema({
  _id: {
    type: mongoose.Schema.ObjectId,
    auto: true,
  },
  animalName: String,
  species: {
    type: mongoose.Schema.Types.ObjectId,
    ref: "Level",
  },
});

export default mongoose.model("Animal", animalModel);
