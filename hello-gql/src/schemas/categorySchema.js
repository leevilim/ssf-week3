import { gql } from "apollo-server-express";

export default gql`
  extend type Query {
    category: Category
  }

  type Category {
    id: ID
    categoryName: String
  }

  extend type Mutation {
    addCategory(categoryName: String): Category
  }
`;
